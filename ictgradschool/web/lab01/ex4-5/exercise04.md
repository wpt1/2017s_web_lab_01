Exercise 04
===========

William Thompson

Welcome to Markdown!

Markdown is a text-to-HTML conversion tool for web writers. 
Markdown allows you to write using an easy-to-read, easy-to-write 
plain text format, then convert it to structurally valid HTML.

Favorite Things
===============

These are a few of my favorite things

* Raindrops on roses

* Whiskers on kittens 

* Bright copper kettles and warm woolen mittens

* Brown paper packages tied up with strings 

* Cream-colored ponies and crisp apple strudels

* Doorbells and sleigh bells 

* Schnitzel with noodles

* Wild geese that fly with the moon on their wings

* Girls in white dresses with blue satin sashes

* Snowflakes that stay on my nose and eyelashes

* Silver-white winters that melt into springs.

