In this exercise, you will simply convert the following collection of elements into an unordered list.


Auckland (North Island)

Hamilton (North Island)

Wellington (North Island)

Christchurch (South Island)

Dunedin (South Island)

Gore (South Island)

Gisborne (North Island)

Napier (North Island)

Taupo (North Island)

Rotorua (North Island)
